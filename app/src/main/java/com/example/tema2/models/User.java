package com.example.tema2.models;

import java.util.ArrayList;

public class User {
    private Cell cellType;
    private int id;
    private String name;
    private String username;
    private String email;
    private boolean post;
    private ArrayList<Post> posts;
    private ArrayList<Album> albums;

    public User(int id, String name, String username, String email){
       this.cellType.setCellType("user");
        this.id=id;
        this.name=name;
        this.username=username;
        this.email=email;
        this.post=false;
        posts=new ArrayList<Post>();
        albums=new ArrayList<Album>();
    }

    public Cell getCellType() {
        return cellType;
    }

    public void setCellType(Cell cellType) {
        this.cellType = cellType;
    }

    public boolean isPost() {
        return post;
    }

    public void setPost(boolean post) {
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }
}
