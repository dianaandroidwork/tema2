package com.example.tema2.models;

import java.util.ArrayList;

public class Album {
    private Cell cellType;
    private int userId;
    private int id;
    private String title;
    private ArrayList<Photo> photos;

    public Album(int userId, int id, String title) {
        this.cellType.setCellType("album");
        this.userId = userId;
        this.id=id;
        this.title=title;
        photos = new ArrayList<Photo>();
    }

    public Cell getCellType() {
        return cellType;
    }

    public void setCellType(Cell cellType) {
        this.cellType = cellType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}
