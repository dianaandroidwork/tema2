package com.example.tema2.models;

public class Cell {
    private String cellType;

    public void setCellType(String cellType) {
        this.cellType = cellType;
    }

    public String getCellType() {
        return cellType;
    }
}

