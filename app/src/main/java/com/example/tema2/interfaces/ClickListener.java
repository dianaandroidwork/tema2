package com.example.tema2.interfaces;

import com.example.tema2.models.Album;
import com.example.tema2.models.User;

public interface ClickListener {
     void userClick(User user);
     void albumCLick(Album album);
     void arrowClick(User user);
}
