package com.example.tema2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema2.R;
import com.example.tema2.interfaces.ClickListener;
import com.example.tema2.models.User;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private ArrayList<User> users;
    private ClickListener clickListener;

    public UserAdapter(ArrayList<User> users, ClickListener clickListener) {
        this.users = users;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.user, parent, false);

        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = users.get(position);
        holder.bind(user);

        boolean postsVisible = users.get(position).isPost();
        holder.postsConstraintLayout.setVisibility(postsVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView userName;
        private TextView info;
        private TextView posts;
        private Button arrowButton;
        private View view;
        private ConstraintLayout postsConstraintLayout;

        public UserViewHolder(View view) {
            super(view);
            this.view = view;
            userName = view.findViewById(R.id.username);
            info = view.findViewById(R.id.information);
            posts = view.findViewById(R.id.postsTextView);
            postsConstraintLayout = view.findViewById(R.id.posts);
            arrowButton = view.findViewById(R.id.arrow);
        }

        public void bind(User user) {
            userName.setText(user.getName());
            info.setText("Username: " + user.getUsername() + "\n" + "Email: " + user.getEmail());

            String userPosts = new String("Posts:\n");
            for(int index=0; index< user.getPosts().size();index++) {
                userPosts+=(index+1)+") "+user.getPosts().get(index).getTitle()+"\n";
            }

            posts.setText(userPosts);

            view.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    clickListener.userClick(user);
                }
            });

            arrowButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    user.setPost(!user.isPost());
                   clickListener.arrowClick(user);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}


